

#include <WiFi.h>
#include <ArduinoWebsockets.h>
#include "driver/i2s.h"
#include "Button2.h";
#include "I2s_Setting.h"
#include "Const.h"
#define RIGHT_BUTTON_PIN  35
#define LED_PIN 9


Button2 rButton = Button2(RIGHT_BUTTON_PIN);
//ezLED led(LED_PIN);  // create ezLED object that attach to pin 9


using namespace websockets;
WebsocketsClient client;

unsigned long timeStampOffset = 0;
unsigned long requestTimestamp;
unsigned long speakingStartTime;

int count =0;
TaskHandle_t i2sReadTaskHandler = NULL;

void setup() {
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);

  button_init();
  i2s_buff_init();
  start_to_connect();
  
  xTaskCreate(ping_task, "ping_task", 2048, NULL, 1, NULL);
}

void loop() {
  rButton.loop();
    //led.loop();

  if (client.available()) {
    client.poll();
  }


 


  
}





void onEventsCallback(WebsocketsEvent event, String data) {
    if(event == WebsocketsEvent::ConnectionClosed) {
        ESP.restart();
    }
}

void onMessageCallback(WebsocketsMessage message) {
    int msgLength = message.length();
    if(message.type() == MessageType::Binary){
      if(states == Listening && msgLength > 0){
        i2s_write_data((char*)message.c_str(), msgLength);
      }

    }else if(message.type() == MessageType::Text){
      unsigned long timeResponse = message.data().toInt();
      actionCommand(timeResponse);
    }
}


void onMessageCallback2(WebsocketsMessage message) {
    int msgLength = message.length();
    if(message.type() == MessageType::Binary){
      Serial.println("binary message is received");
        i2s_write_data((char*)message.c_str(), msgLength);

    }else if(message.type() == MessageType::Text){
      unsigned long timeResponse = message.data().toInt();
      Serial.print("time response:");
      Serial.println(timeResponse);
    }
}


void actionCommand(unsigned long timestamp){
  if(timeStampOffset == 0){
    unsigned long currentTimestamp = millis();
    timeStampOffset = currentTimestamp >= timestamp? currentTimestamp - timestamp : timestamp - currentTimestamp;
    return;
  }

  if(requestTimestamp == timestamp && states == Idle){
    states = Speaking;
    speakingStartTime = millis();  // Start the Speaking mode timer
    Serial.println("* Speaking Mode *");
    i2s_RX_init();
    //led.turnON();
    digitalWrite(LED_PIN, HIGH);

    xTaskCreate(i2s_read_task, "i2s_read_task", 4096, NULL, 1, &i2sReadTaskHandler);
  }else if(requestTimestamp != timestamp ){
    if(states == Idle){
      states = Listening;
      i2s_TX_init();
      Serial.println("* Listening Mode *");
    }else if(states == Listening){
      states = Idle;
      i2s_TX_uninst();
      Serial.println("* IDLE Mode *");
      //led.turnOFF();
      digitalWrite(LED_PIN, LOW);


    }
  }
}

void start_to_connect(){
  WiFi.begin(ssid, password);
//  print_log_screen("- WiFi Connecting");
  Serial.println("- WiFi Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("+ WiFi Connected");
  Serial.println("- Socket Connecting");


  
  client.onMessage(onMessageCallback);
  client.onEvent(onEventsCallback);
  while(!client.connect(websockets_server_host, websockets_server_port, "/")){ // CONNECTION TO WEBSOCKET IS MADE HERE
    delay(500);
    Serial.print(".");
  }

  Serial.println("+ Socket Connected");



 
}

static void i2s_read_task(void *arg){
  while (1) {
      i2s_read_data();
      client.sendBinary((const char*)flash_write_buff, I2S_READ_LEN);
  }
}

static void ping_task(void *arg){
  char dummy_buffer [1];
  while (1) {
    if(states == Idle){
      client.sendBinary(dummy_buffer, 1); 
    }
    vTaskDelay(1000);
  }
}


void playIdleMusic(void *arg) {
  // Custom task code goes here
  while (1) {
    // Task logic

    Serial.print("playing idle music task started");

    if(states == Idle){
      while(!client.connect(websockets_server_host, websockets_server_port, "/")){ // CONNECTION TO WEBSOCKET IS MADE HERE
    delay(500);
    Serial.print(".");
        }
    Serial.println("+ Socket 2 is  Connected");
    client.onMessage(onMessageCallback2);
    }

    
    
    // ...
    
    vTaskDelay(3000); // Delay for 3 seconds
  }
}





void pressed(Button2& btn) {
   if(btn == rButton && states == Idle){
    client.send(getStrTimestamp());
  }
}

void released(Button2& btn) {
  if(btn == rButton && states == Speaking){
    states = Idle;
    Serial.println("* IDLE Mode *");

    delay(100);
    if( i2sReadTaskHandler != NULL ){
      vTaskDelete( i2sReadTaskHandler );
      i2sReadTaskHandler = NULL;
    }
    
    delay(100);      
    client.send(getStrTimestamp());
    i2s_RX_uninst();
  }
}

String getStrTimestamp(){
  requestTimestamp = millis() + timeStampOffset;
  return (String)requestTimestamp;
}

void button_init(){
   rButton.setPressedHandler(pressed);
   rButton.setReleasedHandler(released);
}
